import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from "react-native";
import Constants from "./src/constants/Constants";
import HomeScreen from "./src/screens/HomeScreen";
import DetailContact from "./src/screens/DetailContact";
import Test from "./src/screens/Test"


const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <StatusBar backgroundColor={Constants.color.white}></StatusBar>
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#686AA2'
          },
          headerTintColor: 'white',
        }}
        // screenOptions={{ headerShown: false }}
        initialRouteName='HomeScreen'
      >
        <Stack.Screen name={Constants.screenName.Home} component={HomeScreen} />
        <Stack.Screen name={Constants.screenName.Detail} component={DetailContact} />
        <Stack.Screen name='Test' component={Test} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;