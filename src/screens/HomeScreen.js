import React from "react";
import { SafeAreaView, TextInput, StyleSheet, View, FlatList, Text } from "react-native";
// import { ScrollView } from "react-native-gesture-handler";
import Icon from 'react-native-vector-icons/MaterialIcons';
import PersonItem from "../components/PersonItem";
import Constants from "../constants/Constants";
import person from "../data/data";

const HomeScreen = () => {
    return (
        <SafeAreaView style={{
            backgroundColor: Constants.color.primaryBackground,
            flex: 1,
            flexDirection: 'column'
        }}>
            <View style={styles.contain}>
                {search()}
                <View style={{
                    marginVertical: Constants.mp.mp10
                }}>
                    <FlatList
                        data={person}
                        renderItem={({ item, index }) =>
                            <PersonItem
                                person={item} 
                                index={index} />
                        }
                    />
                </View>
            </View>
        </SafeAreaView>
    )
}

const search = () => {
    return (
        <View style={styles.search}>
            <Icon name="search" size={25} color={Constants.color.grey}></Icon>
            <TextInput placeholder={Constants.string.inputSearch}></TextInput>
        </View>
    )
}

const styles = StyleSheet.create({
    contain: {
        alignContent: 'space-between',
        marginHorizontal: Constants.mp.mp10,
        marginTop: Constants.mp.mp10
    },
    search: {
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: Constants.color.primaryColor,
        borderRadius: Constants.radius.rd10,
        backgroundColor: Constants.color.white,
        paddingHorizontal: Constants.mp.mp10,
    },
    flatlist: {
        // flex: 1,
        // flexDirection: 'column',
        // backgroundColor: Constants.color.white,
        // height: 100
    },
    column: {
        // flexShrink: 1,
    },
    list: {
        // justifyContent: 'space-between'
    },
})

export default HomeScreen;
