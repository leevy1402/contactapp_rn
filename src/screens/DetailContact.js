import React, { react, useState } from "react";
import { Text, StyleSheet, View, Image, Animated, TouchableOpacity, Modal } from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { SafeAreaView } from "react-native-safe-area-context";
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconButton from "../components/IconButton";
import Constants from "../constants/Constants";

const ModalPoup = ({ visible, children }) => {
    const [showModal, setShowModal] = React.useState(visible);
    const scaleValue = React.useRef(new Animated.Value(0)).current;
    React.useEffect(() => {
        toggleModal();
    }, [visible]);
    const toggleModal = () => {
        if (visible) {
            setShowModal(true);
            Animated.spring(scaleValue, {
                toValue: 1,
                duration: 300,
                useNativeDriver: true,
            }).start();
        } else {
            setTimeout(() => setShowModal(false), 200);
            Animated.timing(scaleValue, {
                toValue: 0,
                duration: 300,
                useNativeDriver: true,
            }).start();
        }
    };
    return (
        <Modal transparent visible={showModal}>
            <View style={styles.modalBackGround}>
                <Animated.View
                    style={[styles.modalContainer, { transform: [{ scale: scaleValue }] }]}>
                    {children}
                </Animated.View>
            </View>
        </Modal>
    );
};

const DetailContact = ({ route }) => {
    const { item, index } = route.params;
    const [visible, setVisible] = React.useState(false);
    return (
        <SafeAreaView style={styles.safeArea}>
            {Infor(true)}
            {InfoDetail()}
            <ModalPoup visible={visible}>
                <View style={{ alignItems: 'center' }}>
                    <View style={styles.header}>
                        <TouchableOpacity onPress={() => setVisible(false)}>
                            <Icon name="close" size={30}></Icon>
                        </TouchableOpacity>
                    </View>
                </View>
                {ChangeInf('Change Name: ', item.name)}
                {ChangeInf('Change Phone: ', item.phone)}
                <TouchableOpacity onPress={() => setVisible(false)}>
                    <View style={styles.touchableSave}>
                        <Text style={{ color: Constants.color.white }}>Save</Text>
                    </View>
                </TouchableOpacity>
            </ModalPoup>
        </SafeAreaView >
    )
    function Infor(visible) {
        return (
            <View>
                <View style={styles.briefContact}>
                    <View style={styles.title}>
                        <View style={styles.namephone}>
                            <Text style={styles.name}>
                                {item.name}
                            </Text>
                            <Text style={{ fontSize: Constants.fontSize.default }}>
                                {item.phone}
                            </Text>
                        </View>
                        <TouchableOpacity onPress={() => setVisible(visible)}>
                            <Icon name="edit" size={15}></Icon>
                        </TouchableOpacity>

                    </View>
                    {/* Group Icon */}
                    <View style={{
                        flexDirection: 'row',
                        marginTop: Constants.mp.mp10 * 2,
                    }}>
                        <IconButton sourceImg={Constants.icons.phone}></IconButton>
                        <IconButton sourceImg={Constants.icons.mess}></IconButton>
                        <IconButton sourceImg={Constants.icons.mail}></IconButton>
                    </View>
                </View>
                <View style={styles.contain}>
                    <Image source={item.img} style={styles.img}></Image>
                </View>

            </View>
        )
    }

    function InfoDetail() {
        return (
            <View style={{ marginVertical: Constants.mp.mp10 * 2 }}>
                <BasicInf title={'Dob'} text={item.dob}></BasicInf>
                <BasicInf title={'Address'} text={item.address}></BasicInf>
                <BasicInf title={'Link profile'} text={item.link}></BasicInf>
                <BasicInf title={'Email'} text={item.email}></BasicInf>
            </View>
        )
    }

    function ChangeInf(title, data) {
        return (
            <View>
                <Text>
                    {title}
                </Text>
                <TextInput style={{
                    marginTop: Constants.mp.mp10,
                    paddingLeft: Constants.mp.mp10,
                    borderWidth: 1,
                    borderColor: Constants.color.primaryBackground,
                    borderRadius: Constants.radius.rd10
                }}>
                    {data}
                </TextInput>
            </View>
        )
    }
}

const BasicInf = ({ title, text }) => {
    return (
        <View>
            <Text style={{
                fontSize: Constants.fontSize.default,
                color: Constants.color.greyText
            }}> {title}</Text>
            <Text style={{
                fontWeight: '500',
                fontSize: Constants.fontSize.title,
                paddingLeft: Constants.mp.mp10,
                marginVertical: Constants.mp.mp10,
                color: Constants.color.text
            }}>{text}</Text>
        </View>
    )
}

export default DetailContact;
const styles = StyleSheet.create({
    touchableSave: {
        backgroundColor: Constants.color.primaryColor,
        marginVertical: Constants.mp.mp10 * 2,
        marginHorizontal: Constants.mp.mp10 * 5,
        padding: Constants.mp.mp10,
        alignItems: 'center',
    },
    safeArea: {
        backgroundColor: Constants.color.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        marginHorizontal: Constants.mp.mp10 * 2
    },
    briefContact: {
        backgroundColor: Constants.color.white,
        height: Constants.screen.height / 3,
        marginTop: Constants.mp.mp10 * 8,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: Constants.radius.rd10 * 2
    },
    contain: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    img: {
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
        position: 'absolute',
        resizeMode: 'contain',
        bottom: 180
    },
    title: {
        marginTop: Constants.mp.mp10 * 4,
        flexDirection: 'row',
        alignItems: 'center',
    },
    namephone: {
        paddingRight: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    name: {
        fontWeight: 'bold',
        fontSize: Constants.fontSize.bigText,
        paddingBottom: Constants.mp.mp10
    },

    //new
    modalBackGround: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalContainer: {
        width: '80%',
        backgroundColor: 'white',
        paddingHorizontal: 20,
        paddingVertical: 30,
        borderRadius: 20,
        elevation: 20,
    },
    header: {
        width: '100%',
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },


})