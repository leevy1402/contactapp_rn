import React from "react"
import { StyleSheet, View, Image, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Constants from "../constants/Constants";
import { useNavigation } from '@react-navigation/native';
import DetailContact from "../screens/DetailContact";

const PersonItem = ({ person, index }) => {
    // const handleTapPerson = () => {

    // }
    const navigation = useNavigation();
    return (
        <TouchableOpacity
            onPress={() => navigation.navigate("DetailContact", {
                item: person,
                index: index
            })}
            style={{
                backgroundColor: Constants.color.white,
                marginVertical: Constants.mp.mp10 / 2,
                padding: Constants.mp.mp10,
                flexDirection: 'row',
                borderRadius: Constants.radius.rd10
            }}
        >
            <Image source={person.img} style={styles.img}></Image>
            <View style={{
                paddingLeft: Constants.mp.mp10,
                flexDirection: 'column',
                justifyContent: 'space-between',
                marginVertical: Constants.mp.mp10 / 2,
            }}>
                <Text style={{
                    fontSize: Constants.fontSize.title,
                    fontWeight: 'bold'
                }}>{person.name}</Text>
                <Text style={{
                    fontSize: Constants.fontSize.smallText,
                    color: Constants.color.greyText
                }}>{person.phone}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    img: {
        width: 55,
        height: 55,
        borderRadius: Constants.radius.rd10
    }

})
export default PersonItem;