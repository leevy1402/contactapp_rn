import react from "react";
import { StyleSheet, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

const IconButton = ({ sourceImg }) => {
    return (
        <TouchableOpacity>
            <Image source={sourceImg} style={styles.img}></Image>
        </TouchableOpacity>
    )
}

export default IconButton;

const styles = StyleSheet.create({
    img: {
        width: 80,
        height: 80,
        resizeMode: 'contain',

    }
})