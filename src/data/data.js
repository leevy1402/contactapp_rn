const person = [
    {
        id: '1', name: 'Stacey A. Schuelke', phone: '(+84) 985-519-2268',
        img: require('../../assets/images/avatar1.jpg'),
        address: '3739 Woodland Avenue Metairie, LA 70001',
        email: 'StaceyASchuelke@rhyta.com',
        dob: 'October 19, 1993',
        link: 'luannmancini.com',
        note: ''
    },
    {
        id: '2', name: 'Richard M. Griffin', phone: '(+84) 903-469-4610',
        img: require('../../assets/images/avatar2.jpg'),
        address: '3582 Florence Street Murchison, TX 75778',
        email: 'RichardMGriffin@dayrep.com',
        dob: 'March 17, 1942',
        link: 'phonedialup.com',
        note: ''
    },
    {
        id: '3', name: 'Marshall C. Emmons', phone: '(+84) 530-751-0191',
        img: require('../../assets/images/avatar3.jpg'),
        address: '4897 Byers Lane Yuba City, CA 95991',
        email: 'MarshallCEmmons@rhyta.com',
        dob: 'January 7, 1990',
        link: 'doorrepairmiami.com',
        note: ''
    },
]

export default person;