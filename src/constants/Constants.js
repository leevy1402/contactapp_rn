import { Dimensions } from "react-native";
export default {
    screen: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    },
    screenName: {
        Home: 'Contacts',
        Detail: 'DetailContact'
    },
    color: {
        primaryColor: '#686AA2',
        primaryBackground: '#ecf0f1',
        white: '#fff',
        greyText: '#b2bec3',
        text: '#2f3640'
    },
    string: {
        inputSearch: 'Tìm kiếm...'
    },
    radius: {
        rd10: 10,
        rd15: 15
    },
    mp: {
        mp10: 10
    },
    image: {
        size50: 50
    },
    icons: {
        phone: require('../../assets/icons/phone.jpg'),
        mess: require('../../assets/icons/mess.jpg'),
        mail: require('../../assets/icons/mail.jpg')
    },
    fontSize: {
        default: 14,
        title: 16,
        smallText: 12,
        bigText: 20
    }
}